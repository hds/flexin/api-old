<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $fillable = [
    'id', 'name', 'picture', 'description', 'state', 'category_id', 'infrastructure_id', 'position', 'location_id'
    ];

    public function infrastructure()
    {
        return $this->belongsTo(Infrastructure::class, 'infrastructure_id');
    }

    public function loan()
    {
        return $this->hasOne(Loan::class, 'material_id');
    }

    public function refs()
    {
        return $this->hasMany(MaterialReference::class, 'material_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function parents()
    {
        return $this->belongsToMany(Material::class, 'materials_parents', 'material_id', 'parent_id');
    }

    // public function materials()
    // {
    //     return $this->hasMany(Material::class, 'parent_id');
    // }
}
