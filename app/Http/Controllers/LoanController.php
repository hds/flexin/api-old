<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loan;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Loan::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($loan = Loan::create($request->all())) {
            return response()->json($loan, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($loan = Loan::find($id)) {
            return $loan;
        }
        abort(404, 'Ressource not found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($loan = Loan::find($id)) {
            if ($loan->update($request->all())) {
                return response()->json($loan, 201);
            }
            abort(500, 'Couldn\'t update ressource');
        }
        abort(404, 'Ressource not found');
    }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         * @return \Illuminate\Http\Response
         */
    public function destroy($id)
    {
        $loan = Loan::find($id);
        if ($loan) {
            if ($loan->delete()) {
                abort(200, 'Ressource deleted');
            }
            abort(500, 'Ressource couldn\'t be deleted');
        }
        abort(404, 'Ressource not found');
    }
}
