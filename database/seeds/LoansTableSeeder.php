<?php

use Illuminate\Database\Seeder;
use App\Models\Loan;

class LoansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loans = [
          [
          'material_id' => 1,
          'user_id' => 2,
          'responsible_id' => 1,
          'begin_at' => new DateTime(),
          'end_at' => new DateTime(),
          'deadline_at' => new DateTime(),
          ]
        ];
        foreach ($loans as $key => $loan) {
            Loan::create($loan);
        }
    }
}
