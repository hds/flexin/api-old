<?php
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserReference;
use App\Models\Category;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cats = [
            [
                'id'=>'1',
                'name'=>'Vehicule Autonome',
                'parent_id'=> null,
            ],
        ];
        foreach ($cats as $key => $cat) {
          Category::create($cat);
        }
    }
}
