<?php
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Infrastructure;
class InfrastructuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $infras = [
            [
                'id' => '1',
                'name' => 'Labo Heudyasic',
                'description' => 'UTC',
            ],
        ];
        foreach ($infras as $key => $infra) {
          Infrastructure::create($infra);
        }
    }
}
