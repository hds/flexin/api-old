<?php
use Illuminate\Database\Seeder;
use App\Models\Location;
class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locs = [
            [
                'building'=>'GI',
                'room'=>'0202'
            ],
        ];
        foreach ($locs as $key => $loc) {
          Location::create($loc);
        }
    }
}
